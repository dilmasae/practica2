<footer class=" mt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12 ">
                <div class="row ">
                    <div class="col-md-2">
                        <h2 class="mb-4 text-orange">Manualidades</h2>
                        <ul class="list-unstyled">
                            <a href="#" class="text-white">P�gina principal</a>
                            <br>
                            <a href="#" class="text-white">�Quienes somos?</a>
                        </ul>
                    </div>
                    <div class="col-md-10 cont-relative">
                       <div class="social-network cont-absolute">
                            <a href="https://www.facebook.com" target="_blank"><i class="fab fa-facebook fa-3x text-white"></i></a>
                            <a href="https://twitter.com" target="_blank"><i class="fab fa-twitter fa-3x text-white"></i></a>
                            <a href="https://www.instagram.com" target="_blank"><i class="fab fa-instagram fa-3x text-white"></i></a>
                        </div> 
                    </div>
                </div>
                
            </div>
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-12 mt-3 text-white text-uppercase">
                <p>Contacta con nosotros</p>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d373.90652202534704!2d-15.45207983865615!3d28.07259350540727!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xc40942fde2813a9%3A0x8b61a40c00405a46!2sUniversidad+de+Las+Palmas+de+Gran+Canaria%3A+Escuela+de+Ingenier%C3%ADa+Inform%C3%A1tica!5e0!3m2!1ses!2ses!4v1524411163540" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-6">
                        
                        <form action="#" method="POST" >
                            <div class="form-group">
                                <!--label for="name">Nombre</label>-->
                                <input type="text" class="form-control" id="name" aria-describedby="nombre" placeholder="Escribe tu nombre" required>
                            </div>
                            <div class="form-group">
                                <!--label for="last_name">Apellido</label>-->
                                <input type="text" class="form-control" id="last_name" aria-describedby="apellido" placeholder="Escribe tu apellido" required>
                            </div>
                            <div class="form-group">
                                <!--label for="email">Email</label>-->
                                <input type="email" class="form-control" id="email-footer" aria-describedby="correo" placeholder="Escribe tu correo" required>
                            </div>
                            <div class="form-group">
                                <!--label for="subject">Asunto</label>-->
                                <textarea rows="4" class="form-control" id="subject" placeholder="Escribe tu mensaje" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block">Enviar</button>
                            </fielset
                        </form>
                    </div> 
                </div>
            </div>
            <div class="col-md-12 mt-3">
                <hr>
            </div>
            <div class="col-md-12 mt-3 text-center text-orange">
                <p> Copyright 2018 - Todos los derechos reservados.</p>
            </div>
       </div>
    </div>
</footer>